import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import MyCards from "../components/MyCards";
import { Row, Col } from "react-bootstrap";
import axios from "axios";
function Users() {
  const [users, setUsers] = useState([]);

  // it will only work once , when the component mount
  useEffect(() => {
    const getUser = async () => {
      let response = await axios.get("https://api.escuelajs.co/api/v1/users/");
      setUsers(response.data); 
    };

    getUser();
  }, []);

  console.log("user from the api : ",users)
  return (
    <>

      <div className="container">
        <Row>
          {users.map((user) => (
            <Col key={user.id}>
              <MyCards user={user} />
            </Col>
          ))}
        </Row>
      </div>

      {/* <Outlet/> */}
    </>
  );
}

export default Users;
