import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function MyCards({user}) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={user.avatar} />
      <Card.Body>
        <h3> {user.name}</h3>
        <Card.Title>{user.email}</Card.Title>
        <p className='text-dark fw-bold' >{user.role}</p>

        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default MyCards;